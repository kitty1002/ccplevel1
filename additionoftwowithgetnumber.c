#include<stdio.h>
int get_number();
int get_sum(int a, int b);
int result(int a, int b, int sum);
int main()
{
    int a, b, sum=0 ,answer;
    a = get_number();
    b = get_number();
    sum = get_sum(a, b);
    result(a, b, sum);
    return 0;
}
int get_number()
{
    int a;
    printf("Enter a number\n");
    scanf("%d", &a);
    return a;
}
int get_sum(int a, int b)
{
    int sum;
    sum=a+b;
    return sum;
}
int result(int a, int b, int sum)
{
    printf("sum of two numbers %d and %d is %d", a, b, sum);
}