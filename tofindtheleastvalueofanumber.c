#include<stdio.h>

int get_number();
int smallest(int a, int b, int c);
int result(int a, int b, int c, int sum);

int main()
{
    int a, b,c, lowest=0 ,answer;
    a = get_number();
    b = get_number();
    c = get_number();
    lowest = smallest(a, b, c);
    result(a, b, c, lowest);
    return 0;
}

int get_number()
{
    int p;
    printf("Enter a number\n");
    scanf("%d", &p);
    return p;
}

int smallest(int x, int y, int z)
{
     int smallest  ;
     int array[] = {x,y,z};
     for( int i = 0; i<2 ; i++)
     {
         if(array[i]< array[i+1])
            smallest = array[i];
     }
    return smallest;
}

int result(int a, int b, int c, int sum)
{
    printf("Smallest of %d , %d and %d is %d", a, b, c, sum);
}